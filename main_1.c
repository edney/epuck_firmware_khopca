/* 
 * File:   main.c
 * Author: Edney
 *
 * Created on 6 de Setembro de 2014, 21:42
 */

#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include "e_bluetooth.h"
#include "e_uart_char.h"
#include "e_epuck_ports.h"
#include "e_init_port.h"
#include "e_led.h"


#define DELAY 50000

//#define uart2_send_static_text(msg) { e_send_uart2_char(msg,sizeof(msg)-1); while(e_uart2_sending()); }
#define uart_send_static_text(msg) do { e_send_uart1_char(msg,sizeof(msg)-1); while(e_uart1_sending()); } while(0)
#define uart_send_text(msg) do { e_send_uart1_char(msg,strlen(msg)); while(e_uart1_sending()); } while(0)

int getselector() {
	return SELECTOR0 + 2*SELECTOR1 + 4*SELECTOR2 + 8*SELECTOR3;
}

void wait(long num) {
	long i;
	for(i=0;i<num;i++);
}

/*
 * 
 */
int main(void) {

    char message[50];
    char command[30];
    char response[80];
    int c;
    int i,version,j;
    int nb_bt_device,error;//max 10

    e_init_port();
    e_init_uart1();
    //e_init_uart2();

    if(RCONbits.POR)	//Reset if Power on (some problem for few robots)
	 {
		RCONbits.POR=0;
		__asm__ volatile ("reset");
	 }

    if(getselector()==0){
       sprintf(message, "\n\n\rWELCOME to the protocol on e-Puck Bluetooth calibration");
       e_send_uart1_char(message,strlen(message));  //send string
       sprintf(response, "\n\rPress H (return) for help");
       e_send_uart1_char(response,strlen(response));  //send string

      while(1)
		{
		   i = 0;
                   c=0;
		   do
		   {
		     	if (e_getchar_uart1(&command[i]))
				{
					c=command[i];
		    		i++;
				}
		   }
		   while (((char)c != '\n')&&((char)c != '\x0d'));
		   command[i]='\0';

		   switch (command[0])
		   {
		   	case 'A':	sprintf(message,"\n\rSearch begin");
						e_send_uart1_char(message,strlen(message));
                                                e_bt_exit_tranparent_mode();
						nb_bt_device=e_bt_find_epuck();
                                                e_bt_tranparent_mode();

                                        sprintf(response,"quantidade epucks = %d",nb_bt_device);
                                        e_send_uart1_char(response,strlen(response));
					while(e_uart1_sending());
                                        
                                                
						for(j=0;j<nb_bt_device;j++){
							sprintf(response,"\n\r%d. e_puck_%c%c%c%c BT_address:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x",j,(unsigned char)e_bt_present_epuck[j].number[0],(unsigned char)e_bt_present_epuck[j].number[1],(unsigned char)e_bt_present_epuck[j].number[2],(unsigned char)e_bt_present_epuck[j].number[3],(unsigned char)e_bt_present_epuck[j].address[0],(unsigned char)e_bt_present_epuck[j].address[1],( unsigned char)e_bt_present_epuck[j].address[2],( unsigned char)e_bt_present_epuck[j].address[3],( unsigned char)e_bt_present_epuck[j].address[4],(unsigned char)e_bt_present_epuck[j].address[5]);
							e_send_uart1_char(response,strlen(response));
							while(e_uart1_sending());
						}
						sprintf(response,"\n\rInquiry finished");
						break;
			case 'B':	sprintf(message,"\n\rSearch e-puck");
						e_send_uart1_char(message,strlen(message));
                                                 e_bt_exit_tranparent_mode();
						if(e_bt_connect_epuck()==0)
							sprintf(response,"\n\rc, connected");
						else
							sprintf(response,"\n\rc, connection failed");
						 e_bt_tranparent_mode();
                                                 break;
			case 'P':               e_bt_exit_tranparent_mode();
                                                e_bt_read_local_pin_number(message);
                                                 e_bt_tranparent_mode();
						sprintf(response,"\n\rPIN code = %s",message);
						break;
			case 'O':	sscanf(command,"O,%s\n",message);
                                                e_bt_exit_tranparent_mode();
						if(e_bt_write_local_pin_number(message))
							sprintf(response,"\n\rError writting PIN");
						else
							sprintf(response,"\n\rPIN code = %s",message);
                                                e_bt_tranparent_mode();
						break;
			case 'M':	sscanf(command,"M,%s\n",message);
                                                e_bt_exit_tranparent_mode();
						if(e_bt_write_local_name(message))
							sprintf(response,"\n\rError writting Name");
						else
							sprintf(response,"\n\rFriendly name = %s",message);
                                                e_bt_tranparent_mode();
						break;
			case 'S':	sscanf(command,"S,%s\n",message);
                                                e_bt_exit_tranparent_mode();
						if(e_bt_write_local_pin_number(message))
							sprintf(response,"\n\rError writting PIN");
						else
							sprintf(response,"\n\rPIN code = %s",message);
                                                e_bt_tranparent_mode();
						e_send_uart1_char(response,strlen(response));
						sprintf(command,"e-puck_%s",message);
						if(e_bt_write_local_name(command))
							sprintf(response,"\n\rError writting Name");
						else
							sprintf(response,"\n\rFriendly name = %s",command);
						break;
			case 'N':	e_bt_exit_tranparent_mode();
                                        e_bt_read_local_name(message);
                                        e_bt_tranparent_mode();
						sprintf(response,"\n\rFriendly name = %s",message);
						break;
			case 'R':	version=e_bt_reset();
						sprintf(response,"\n\rReset ok Firmware = %d",version);
						break;
			case 'H':	uart_send_static_text("\n\r \"A\" find e-puck");
						uart_send_static_text("\n\r \"B\" connect to nearest e-puck");
						uart_send_static_text("\n\r \"C,#\" Connect SPP device # (make inquiry first)");
						uart_send_static_text("\n\r \"D,\%s\" Send data");
						uart_send_static_text("\n\r \"E\" Release SPP conection");
						uart_send_static_text("\n\r \"F,#\" Ask friendly name local device # (make inquiry first)");
						uart_send_static_text("\n\r \"I\" Inquiry local device");
						uart_send_static_text("\n\r \"K\" List local paired device");
						uart_send_static_text("\n\r \"L,#\" Remove pairing device device # (make list local paired device first)");
						uart_send_static_text("\n\r \"M,Name\" Write Name for Friendly Bluetooth name");
						uart_send_static_text("\n\r \"N\" Read actual Friendly Bluetooth name");
						uart_send_static_text("\n\r \"O,#\" Write # PIN number");
						uart_send_static_text("\n\r \"P\" Read actual PIN number");
						uart_send_static_text("\n\r \"R\" Soft reset Bluetooth module");
						uart_send_static_text("\n\r \"S,#\" Write # PIN number and same time e-puck_#");
						uart_send_static_text("\n\r \"T\" Enter transparent mode");
						uart_send_static_text("\n\r \"U\" Exit transparent mode");

						response[0]='\n';
						response[1]='\0';
						break;
			case 'I':	sprintf(message,"\n\rInquiry begin");
						e_send_uart1_char(message,strlen(message));
                                                 e_bt_exit_tranparent_mode();
						nb_bt_device=e_bt_inquiry(e_bt_present_device);
                                                 e_bt_tranparent_mode();
						for(j=0;j<nb_bt_device;j++){
							sprintf(response,"\n\rDevice %d BT_address:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x Type:%2.2x:%2.2x:%2.2x",j,(unsigned char)e_bt_present_device[j].address[0],(unsigned char)e_bt_present_device[j].address[1],( unsigned char)e_bt_present_device[j].address[2],( unsigned char)e_bt_present_device[j].address[3],( unsigned char)e_bt_present_device[j].address[4],(unsigned char)e_bt_present_device[j].address[5],(unsigned char)e_bt_present_device[j].class[0],(unsigned char)e_bt_present_device[j].class[1],(unsigned char)e_bt_present_device[j].class[2]);
							e_send_uart1_char(response,strlen(response));
							while(e_uart1_sending());
						}
						sprintf(response,"\n\rInquiry finished");
						break;
			case 'F':	sscanf(command,"F,%d\r",&j);
						e_bt_exit_tranparent_mode();
                                                e_bt_get_friendly_name(&e_bt_present_device[j]);
                                                e_bt_tranparent_mode();
						sprintf(response,"\n\rBT_address:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x Friendly Name:%s",(unsigned char)e_bt_present_device[j].address[0],(unsigned char)e_bt_present_device[j].address[1],( unsigned char)e_bt_present_device[j].address[2],( unsigned char)e_bt_present_device[j].address[3],( unsigned char)e_bt_present_device[j].address[4],(unsigned char)e_bt_present_device[j].address[5],&e_bt_present_device[j].friendly_name[0]);
						break;
			case 'K':	e_bt_exit_tranparent_mode();
                                        nb_bt_device=e_bt_list_local_paired_device();
                                        e_bt_tranparent_mode();
						for(j=0;j<nb_bt_device;j++){
							sprintf(response,"\n\rDevice %d BT_address:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x",j,(unsigned char)e_bt_local_paired_device[0+j*6],(unsigned char)e_bt_local_paired_device[1+j*6],( unsigned char)e_bt_local_paired_device[2+j*6],( unsigned char)e_bt_local_paired_device[3+j*6],( unsigned char)e_bt_local_paired_device[4+j*6],(unsigned char)e_bt_local_paired_device[5+j*6]);
							e_send_uart1_char(response,strlen(response));
							while(e_uart1_sending());
							}
						response[0]='\n';
						response[1]='\0';
						break;
			case 'L':	sscanf(command,"L,%d\r",&j);
                                                e_bt_exit_tranparent_mode();
						e_bt_remove_local_paired_device(j);
                                                e_bt_tranparent_mode();
						sprintf(response,"\n\rl,erase paring");
						break;
			case 'D':	sscanf(command,"D,%s\r",&message);
						e_bt_exit_tranparent_mode();
                                                e_bt_send_SPP_data(message,strlen(message));
                                                e_bt_tranparent_mode();
						sprintf(response,"\n\rd,data send");
						break;
			case 'C':	sscanf(command,"C,%d\r",&j);
                                                e_bt_exit_tranparent_mode();
						if(e_bt_etablish_SPP_link(&e_bt_present_device[j].address[0])==0)
							sprintf(response,"\n\rc, connected");
						else
							sprintf(response,"\n\rc, connection failed");
                                                e_bt_tranparent_mode();
						break;
			case 'E':	e_bt_exit_tranparent_mode();
                                        if(e_bt_release_SPP_link()==0x1f)
							sprintf(response,"\n\ry, no connection");
						else
							sprintf(response,"\n\ry, released");
                                                e_bt_tranparent_mode();
						break;
			case 'T':	e_bt_tranparent_mode();
						sprintf(response,"\n\rt, tranparent mode");
						break;
			case 'U':	e_bt_exit_tranparent_mode();
						sprintf(response,"\n\ru, exit tranparent mode");
						break;
			case 'X':	e_bt_factory_reset();
						sprintf(response,"\n\rx, factory reset");
						break;
		   	default:    sprintf(response,"\n\rz,Command not found");
		                 	break;
		   }
		   e_send_uart1_char(response,strlen(response));
		   while(e_uart1_sending());
		 }

    }else if(getselector()==4){
        LED1 = 1;
        e_bt_exit_tranparent_mode();

        int qtd = e_bt_find_epuck();

        if(qtd == 0)
        {
            LED5 = 1;

        }
        else{
            LED6 = 1;
        }


        LED1 = 0;

        e_bt_tranparent_mode();

    } else if(getselector() == 5) {


        e_bt_exit_tranparent_mode();
        e_bt_reset();
        e_set_led(0,1);
         int flag = 9;
       
		
               error=e_bt_connect_epuck();
              
               
              if(error==0){

                  e_set_led(7,1);

			e_bt_tranparent_mode();
			//run_breitenberg();

                        char buffer[80];
                      


                        while (1) {


                            if(flag == 8){
                                
                                sprintf(buffer, "%d\n",2);
                                e_send_uart1_char(buffer, strlen(buffer));
                                while(e_uart1_sending());
                                flag = 9;
                                wait(100000);
                                e_set_led(2,1);
                            }
                            if(flag == 9){
                                sprintf(buffer, "%d\n",3);
                                e_send_uart1_char(buffer, strlen(buffer));
                                while(e_uart1_sending());
                                flag = 8;
                                wait(100000);
                                e_set_led(2,0);
                            }
                               
                                
                        }

                        e_bt_exit_tranparent_mode();


		}
		




    } else if(getselector() == 6) {


        e_bt_tranparent_mode();
        char received[32];
        while(1){

            i = 0;
            c = 0;

            do{
                if (e_getchar_uart1(&received[i])){
                    c=received[i];
		    i++;
                    e_set_led(7,1);
                }

            }while (((char)c != '\n')&&((char)c != '\x0d'));
            received[i]='\0';

            if(received[0] == '2'){
                e_set_led(3,1);
            }else if(received[0] == '3'){
                e_set_led(3,0);
            }else{

            }
        }
        e_bt_exit_tranparent_mode();

    }else if(getselector() == 7){

        e_bt_tranparent_mode();
        char received[32];
        char buffer2[32];
        int flag2 = 9;
        
       

        while(1){

            i = 0;
            c = 0;

            do{
                if (e_getchar_uart1(&received[i])){
                    c=received[i];
		    i++;
                    e_set_led(7,1);
                }

            }while (((char)c != '\n')&&((char)c != '\x0d'));
            received[i]='\0';


            if(received[0] == '4'){
                sprintf(buffer2, "3040-0001-0001-00024500-00018700",2);
                e_send_uart1_char(buffer2, strlen(buffer2));
                while(e_uart1_sending());
            }



            if(received[0] == '8'){

                int count = 0;
                e_set_led(0,1);
                e_bt_exit_tranparent_mode();

                error=e_bt_connect_epuck();

                if(error==0){
                    e_bt_tranparent_mode();

                    while (count < 50) {


                            if(flag2 == 8){

                                sprintf(buffer2, "%d\n",2);
                                e_send_uart1_char(buffer2, strlen(buffer2));
                                while(e_uart1_sending());
                                flag2 = 9;
                                wait(100000);
                                e_set_led(2,1);
                            }
                            if(flag2 == 9){
                                sprintf(buffer2, "%d\n",3);
                                e_send_uart1_char(buffer2, strlen(buffer2));
                                while(e_uart1_sending());
                                flag2 = 8;
                                wait(100000);
                                e_set_led(2,0);
                            }

                         count++;
                        }
                         e_bt_exit_tranparent_mode();


                }



                e_set_led(0,0);
                e_bt_release_SPP_link();



               // e_bt_etablish_SPP_link(e_bt_present_device[0].address);


            }else if(received[0] == '3'){
                e_set_led(3,0);
            }else{

            }
        }
        e_bt_exit_tranparent_mode();



    }else{


        LED0=1;
		error=e_bt_connect_epuck();
		if(error==0){
			e_bt_tranparent_mode();
                        LED0=1;
                        LED1=1;
                        LED2=1;
                        LED3=1;
                        LED4=1;
                        LED5=1;
		}
		LED3=1;


    }


    LED0=0;

		while(1)
		{
			LED4!=LED4;
			for(j=0;j<30000;j++);
		}


  return 1;
}

