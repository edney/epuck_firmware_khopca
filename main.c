#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include "e_bluetooth.h"
#include "e_uart_char.h"
#include "e_epuck_ports.h"
#include "e_init_port.h"
#include "e_led.h"
#include "e_ad_conv.h"
#include "e_prox.h"
#include "e_motors.h"
#include "utility.h"

#define SIZE_BT_BUFFER 10
#define MAX_VALUE_K 3
#define MIN_VALUE_K 0
#define BIAS_SPEED  500
#define MAX_SPEED   800

char msg_in[SIZE_BT_BUFFER - 4];
char msg_out[SIZE_BT_BUFFER - 4];
char running;
char typeMobility;
int calibrated[8];
int proximityValue;
int quantityOfNeighbors;
int neighbors[99];
int IDs[99];
int indexMin;
static int turnValue;
int maxvalue;
int kValue;
int IDValue;

int primeiro;
int segundo;

int minValue(int* array, int size){
    int val = array[0];
    int i;
    for (i = 1; i < size; ++i){
        val = val <= array[i] ? val : array[i];
    }
    return val;
}

int getIndexMinNeighbor(int* array, int size){

    int position = 0;
    int val = array[0];
    int i;
    for (i = 1; i < size; ++i){
        if(array[i] <= val){
            val = array[i];
            position = i;
        }
    }
    return position;
}

int khopca(){

    int resultValue;
    int indexMinNeighbor;

    if(minValue(neighbors, quantityOfNeighbors) < kValue){
        resultValue = minValue(neighbors, quantityOfNeighbors) + 1;
       
    }
    else if ((minValue(neighbors, quantityOfNeighbors) == MAX_VALUE_K) && (kValue == MAX_VALUE_K) ){
        resultValue = MIN_VALUE_K;
        
    }
    else if ((minValue(neighbors, quantityOfNeighbors) >= kValue) && (kValue != 0)){
        resultValue = kValue + 1;
        
    }
    else if ((minValue(neighbors, quantityOfNeighbors) == 0) &&  (kValue == 0)){
        indexMinNeighbor = getIndexMinNeighbor(neighbors, quantityOfNeighbors);
        indexMin = indexMinNeighbor;

        if(IDs[indexMinNeighbor] > IDValue ){
            resultValue = kValue + 1;
            primeiro = IDs[indexMinNeighbor];
            segundo = IDValue;
            
        }
        else{
            resultValue = kValue;
            
        }
    }
    else{
        resultValue = kValue;
       
    }
    return resultValue;
}

int roundValue(double nb){
   
    if(nb > 0){
	return (int) (nb+0.5);
    }
    else{
	return (int) (nb-0.5);
    }
    return (int) nb;
}

void calibrateIR() {
    int i, j;
    long sensor[8];

    for (i = 0; i < 8; i++) {
        sensor[i] = 0;
        calibrated[i] = 0;
    }

    for (j = 0; j < 32; j++) {
        for (i = 0; i < 8; i++)
            sensor[i] += e_get_prox(i);
        wait(10000);
    }

    for (i = 0; i < 8; i++) {
        calibrated[i] = sensor[i] / 32;
    }
}

void avoidCollision(){

    if (e_get_prox(0) - calibrated[0] > proximityValue || e_get_prox(1) - calibrated[1] > proximityValue || e_get_prox(2) - calibrated[2] > proximityValue) {

        e_set_speed_left(-300);
        e_set_speed_right(300);
    }

    else if (e_get_prox(5) - calibrated[5] > proximityValue || e_get_prox(6) - calibrated[6] > proximityValue || e_get_prox(7) - calibrated[7] > proximityValue ) {

        e_set_speed_left(300);
        e_set_speed_right(-300);
    }

    else {

        e_set_speed_left(100);
        e_set_speed_right(100);
    }
}

void updateMotors(){

    if (running == 1) {
        avoidCollision();
    }else{
        e_set_speed_left(0);
	e_set_speed_right(0);
    }
}

void receiveBluetoothMessage(){
    char bt_buffer_in[10];
    
    while (1){
        int count = 0;
        while(!e_ischar_uart1())
            NOP();
        e_getchar_uart1(&bt_buffer_in[count]);

        if(bt_buffer_in[count] != 0x16)
            continue;
        count = 1;

        while(!e_ischar_uart1())
            NOP();
        e_getchar_uart1(&bt_buffer_in[count]);

        if(bt_buffer_in[count] != 0x02)
            continue;

        do {
            while(!e_ischar_uart1())
                NOP();
            count++;
            e_getchar_uart1(&bt_buffer_in[count]);
        } while (bt_buffer_in[count] != 0x03);

        while(!e_ischar_uart1())
            NOP();
        e_getchar_uart1(&bt_buffer_in[count+1]);

        if(bt_buffer_in[count+1] != 0x17)
            continue;

        int j;
        for(j=0; j < 6; j++){
            msg_in[j] = bt_buffer_in[j+2];
        }
        break;
    }
}

void sendBluetoothMessage(){
     if (!strncmp(&msg_in[0], "000001",6)) { //Get ID
        sprintf(msg_out, "002979");
        IDValue = 2979;
        e_send_uart1_char(msg_out, strlen(msg_out));
        while(e_uart1_sending());
     }

     if (!strncmp(&msg_in[0], "000002",6)) { //Get old K
        sprintf(msg_out, "000009");
        e_send_uart1_char(msg_out, strlen(msg_out));
        while(e_uart1_sending());
     }

     if (!strncmp(&msg_in[0], "6",1)) { //Get K

        if(quantityOfNeighbors != 0){

            int resultToSend = khopca();
            kValue = resultToSend;
            sprintf(msg_out, "00000%d",resultToSend); //Fazer um condicional para verificar quantos digitos tem o numero a ser enviado pela serial,
                                                         //garantindo assim que ser� enviado sempre 6 caracteres
            e_send_uart1_char(msg_out, strlen(msg_out));
            while(e_uart1_sending());
         }else{
            sprintf(msg_out, "00000%d",0);
            kValue = MIN_VALUE_K;
            e_send_uart1_char(msg_out, strlen(msg_out));
            while(e_uart1_sending());
         }
    }

    if (!strncmp(&msg_in[0], "5",1)) {
        sprintf(msg_out, "00000%d",0);
        kValue = MIN_VALUE_K;
        e_send_uart1_char(msg_out, strlen(msg_out));
        while(e_uart1_sending());
    }

    if (!strncmp(&msg_in[0], "7",1)) {
        sprintf(msg_out, "00%d",primeiro);
        e_send_uart1_char(msg_out, strlen(msg_out));
        while(e_uart1_sending());
    }

    if (!strncmp(&msg_in[0], "8",1)) {
        sprintf(msg_out, "00%d",segundo);
        e_send_uart1_char(msg_out, strlen(msg_out));
        while(e_uart1_sending());
    }


}

void updateControls(){
     
     if (!strncmp(&msg_in[0], "100001",6)) { //Start with avoidCollision
        running = 1;
        typeMobility = msg_in[1];
     }

     if (!strncmp(&msg_in[0], "100002",6)) { //Stop
        running = 0;
     }
   
     if (!strncmp(&msg_in[0], "3",1)) { //update proximity value

          char temp[4];
          temp[0] = msg_in[3];
          temp[1] = msg_in[4];
          temp[2] = msg_in[5];
          temp[3] ='\0';
          proximityValue = atoi(temp);
     }

     updateMotors();
}

// timer 1 interrupt
// this code is executed every UPDATE_TIMING ms
void __attribute__((interrupt, auto_psv, shadow)) _T1Interrupt(void) {
    IFS0bits.T1IF = 0;                      // clear interrupt flag
    updateControls();
}

/* init the Timer 1 */
void InitTMR1(void) {
    T1CON = 0;
    T1CONbits.TCKPS = 3;                    // prescsaler = 256
    TMR1 = 0;                               // clear timer 1
    PR1 = (50*MILLISEC)/256.0;   // interrupt after UPDATE_TIMING ms
    IFS0bits.T1IF = 0;                      // clear interrupt flag
    IEC0bits.T1IE = 1;                      // set interrupt enable bit
    T1CONbits.TON = 1;                      // start Timer1
}

int main(void) {

    IDValue = 2979;
    e_init_port();
    e_init_uart1();
   
    e_init_ad_scan(ALL_ADC);
    proximityValue = 250;
    maxvalue = 9;
    kValue = rand() % 4;

    turnValue = roundValue(53000*90/(360*41));

    if(RCONbits.POR){
	RCONbits.POR=0;
	__asm__ volatile ("reset");
    }

    InitTMR1();
    calibrateIR();
    
    if(getselector() == 2) {

        while (1){
            if(e_ischar_uart1()){
                quantityOfNeighbors = 0;

                char status = running;
                receiveBluetoothMessage();
                if(msg_in[0] == '0')
                    sendBluetoothMessage();

                if(msg_in[0] == '7')
                    sendBluetoothMessage();
                
                if(msg_in[0] == '8')
                    sendBluetoothMessage();

                if(msg_in[0] == '5'){
                    char tempQNeighbors[4] = "";
                    tempQNeighbors[0] = msg_in[3];
                    tempQNeighbors[1] = msg_in[4];
                    tempQNeighbors[2] = msg_in[5];
                    tempQNeighbors[3] = '\0';

                    quantityOfNeighbors = atoi(tempQNeighbors);

                    if(quantityOfNeighbors != 0){
                        int i;
                        for(i=0; i < quantityOfNeighbors; i++){

                            receiveBluetoothMessage();
                            char tempNeighborsID[5] = "";
                            tempNeighborsID[0] = msg_in[2];
                            tempNeighborsID[1] = msg_in[3];
                            tempNeighborsID[2] = msg_in[4];
                            tempNeighborsID[3] = msg_in[5];
                            tempNeighborsID[4] = '\0';
                            IDs[i] = atoi(tempNeighborsID);
                            wait(25);

                            receiveBluetoothMessage();
                            char tempNeighbors[4] = "";
                            tempNeighbors[0] = msg_in[3];
                            tempNeighbors[1] = msg_in[4];
                            tempNeighbors[2] = msg_in[5];
                            tempNeighbors[3] = '\0';
                            neighbors[i] = atoi(tempNeighbors);
                            wait(25);
                        }
                    }
                    sendBluetoothMessage();
                }
                running = status;
            }
        }
    }else{
        NOP();
    }
  return 1;
}